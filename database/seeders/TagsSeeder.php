<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Tag;

class TagsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tag::create(['name' => 'Sports']);
        Tag::create(['name' => 'Food']);
        Tag::create(['name' => 'Fashion']);
        Tag::create(['name' => 'Leisure']);
        Tag::create(['name' => 'Health']);
        Tag::create(['name' => 'Politic']);
        Tag::create(['name' => 'Decoration']);
        Tag::create(['name' => 'Finance']);
    }
}
