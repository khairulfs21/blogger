<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tag;
use App\Models\Post;

class PostController extends Controller
{
    public function create()
    {
        $tags = Tag::get();

        return view("post.create", compact('tags'));
    }

    public function store(Request $request)
    {

        $post = Post::create([
            'title' => $request->title,
            'body' => $request->body,
            'user_id' => auth()->id(),
        ]);

        $post->tags()->attach($request->tags);

        return redirect()->route("home");
    }

    public function show($id)
    {
        $post = Post::where('id', $id)->first();

        return view("post.show", compact('post'));
    }
}
