<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comment;

class CommentController extends Controller
{
    public function store($id_post, Request $request)
    {
        Comment::create([
            'user_id' => auth()->user()->id,
            'post_id' => $id_post,
            'body' => $request->comment,
        ]);

        return back();
    }
}
