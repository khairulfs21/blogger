@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
            @foreach ($posts as $post)
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-header">
                            @foreach ($post->tags as $tag)
                                <span class="badge badge-primary">{{  $tag->name }}</span>
                            @endforeach
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">{{ $post->title }}</h5>
                            <p class="card-text">{{  substr($post->body,0,50) }}</p>
                            <p>author : {{  $post->user->name }}</p>
                            <a href="{{  route("post.show", $post->id) }}" class="btn btn-danger">Read More..</a>
                        </div>
                    </div>
                </div>
            @endforeach
    </div>
</div>
@endsection
