@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 mb-3">
            <div class="card">
                <div class="card-body">
                    <h3>
                        {{  $post->title }}
                        <span class="float-right">
                            <a href="{{ route("welcome") }}" class="btn btn-info">Back</a>
                        </span>
                    </h3>
                    <span>Author by: {{ $post->user->name }}</span>
                    <br>
                    <small>
                        @foreach ($post->tags as $tag)
                            <span class="badge badge-primary">{{  $tag->name }}</span>
                        @endforeach
                    </small>
                    <section>
                        {{ $post->body }}
                    </section>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            @auth
            <div class="card mt-3">
                <div class="card-body">
                    <form action="{{ route("comment.store", $post->id) }}" method="post">
                        @csrf
                        <div class="form-group">
                          <label>Comment Here</label>
                          <textarea name="comment" class="form-control" cols="30" rows="10"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Add Comment</button>
                    </form>
                </div>
            </div>
            @else
              Please <a href="{{ route("login") }}"></a> To Comment
            @endauth
            @if(is_null($post->comments))
                        <h3>No Comment</h3>
            @else
                @foreach ($post->comments as $comment)
                <div class="card mt-3">
                    <div class="card-body">
                        {{ $comment->user->name }} said at {{  $comment->created_at->diffForHumans() }} <br>
                        <hr>
                        {{  $comment->body }}
                    </div>
                </div>
                @endforeach
            @endif
        </div>

    </div>
</div>
@endsection
