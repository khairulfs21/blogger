@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 mb-3">
            <div class="card">
                <div class="card-body">
                    <a href="{{  route("post.create") }}" class="btn btn-primary float-right">New Post</a>
                </div>
            </div>
            <div class="card mt-3">
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Author By</th>
                                <th>Tags</th>
                                <th>Created At</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($posts as $post)
                            <tr>
                                <td scope="row">{{  $post->title }}</td>
                                <td>{{  $post->user->name }}</td>
                                <td>{{  $post->tags->implode('name',', ') }}</td>
                                <td>{{  $post->created_at->diffForHumans() }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


            {{-- <div class="col-md-3">
                <div class="card">
                    <div class="card-body">
                    <h5 class="card-title">{{ $post->title }}</h5>
                    <p class="card-text"></p>
                    </div>
                </div>
            </div> --}}

    </div>
</div>
@endsection
